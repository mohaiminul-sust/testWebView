//
//  ViewController.swift
//  testWebView
//
//  Created by Mohaiminul Islam on 3/23/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {
    
    let url = "https://www.fivesimplesteps.com/"
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        let requestUrl = NSURL(string:url)
        let request = NSURLRequest(URL: requestUrl!)
        
        webView.delegate = self
        webView.loadRequest(request)
        webView.scalesPageToFit = true
    }
    
    //Delegates: webView delegates
    func webViewDidStartLoad(webView : UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(webView : UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print("webView failed with error \(error)")
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }

}

